import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, ref as refS, onValue } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";

const firebaseConfig = {
  apiKey: "AIzaSyBnkQAoCsmxJDoSITf9Bb833hOU9y17XCs",
  authDomain: "aplicacionweb-5d84b.firebaseapp.com",
  databaseURL: "https://aplicacionweb-5d84b-default-rtdb.firebaseio.com",
  projectId: "aplicacionweb-5d84b",
  storageBucket: "aplicacionweb-5d84b.appspot.com",
  messagingSenderId: "930174899745",
  appId: "1:930174899745:web:3ce0cc0d3775d86508b560"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

// Obtener referencia a la tabla de productos
const productosRef = refS(db, 'productos');

// Escuchar cambios en la base de datos
onValue(productosRef, (snapshot) => {
  // Limpiar la tabla antes de mostrar los nuevos datos
  limpiarTabla();

  // Recorrer los datos de la tabla productos
  snapshot.forEach((childSnapshot) => {
    const data = childSnapshot.val();
    
    // Mostrar los datos en la tabla
    mostrarEnTabla(data.nombre, data.precio, data.stock, data.url);
  });
});

// Función para mostrar datos en la tabla
function mostrarEnTabla(nombre, precio, stock, url) {
  const tabla = document.getElementById('tablaProductos');
  const fila = tabla.insertRow();
  
  const celdaNombre = fila.insertCell(0);
  const celdaPrecio = fila.insertCell(1);
  const celdaStock = fila.insertCell(2);
  const celdaUrl = fila.insertCell(3);

  celdaNombre.textContent = nombre;
  celdaPrecio.textContent = precio;
  celdaStock.textContent = stock;
  celdaUrl.textContent = url;
}

// Función para limpiar la tabla
function limpiarTabla() {
  const tabla = document.getElementById('tablaProductos');
  while (tabla.rows.length > 1) {
    tabla.deleteRow(1);
  }
}
