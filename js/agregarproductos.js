import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";

const firebaseConfig = {
  apiKey: "AIzaSyBnkQAoCsmxJDoSITf9Bb833hOU9y17XCs",
  authDomain: "aplicacionweb-5d84b.firebaseapp.com",
  databaseURL: "https://aplicacionweb-5d84b-default-rtdb.firebaseio.com",
  projectId: "aplicacionweb-5d84b",
  storageBucket: "aplicacionweb-5d84b.appspot.com",
  messagingSenderId: "930174899745",
  appId: "1:930174899745:web:3ce0cc0d3775d86508b560"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

window.addEventListener('DOMContentLoaded', (event) => {
  mostrarProductos();
});

var btnAgregar = document.getElementById('btnAgregar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');

var productoId = "";
var productoNombre = "";
var productoPrecio = "";
var productoStock = "";
var productoURL = "";

function leerInputs() {
productoId = document.getElementById('id').value; 
  productoNombre = document.getElementById('nombre').value;
  productoPrecio = document.getElementById('precio').value;
  productoStock = document.getElementById('stock').value;
  productoURL = document.getElementById('url').value;
}

function insertarProducto() {
  leerInputs();
  set(refS(db, 'productos/' + productoId), {
    nombre: productoNombre,
    precio: productoPrecio,
    stock: productoStock,
    url: productoURL
  }).then(() => {
    alert("Producto agregado con éxito");
  }).catch((error) => {
    alert("Ocurrió un error al agregar el producto: " + error);
  });
  limpiarInputs();
  mostrarProductos()
}



function buscarProducto() {
  const id = document.getElementById('id').value;
  const dbRef = refS(db, 'productos/' + id);

  get(dbRef).then((snapshot) => {
    if (snapshot.exists()) {
      const data = snapshot.val();
      productoId = id; // Asignar el valor del ID correctamente
      productoNombre = data.nombre;
      productoPrecio = data.precio;
      productoStock = data.stock;
      productoURL = data.url;
      escribirInputs();
    } 
  }).catch((error) => {
    alert(error);
  });
}

function mostrarProductos() {
    const dbRef = refS(db, 'productos');
    const tabla = document.getElementById('productos');
    const tbody = tabla.querySelector('tbody');
    
    tbody.innerHTML = '';
  
    const nombresCeldas = ['id', 'nombre', 'precio', 'stock', 'url'];
  
    const encabezado = document.createElement('tr');
    nombresCeldas.forEach(nombre => {
      const celda = document.createElement('th');
      celda.textContent = nombre;
      encabezado.appendChild(celda);
    });
    tbody.appendChild(encabezado);
  
    onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const key = childSnapshot.key;
        const data = childSnapshot.val();
  
        var fila = document.createElement('tr');
  
        nombresCeldas.forEach(nombre => {
          var celda = document.createElement('td');
          celda.textContent = (nombre === 'id') ? key : data[nombre];
          fila.appendChild(celda);
        });
  
        tbody.appendChild(fila);
      });
    }, { onlyOnce: true });
  }
  

function actualizarProducto() {
    leerInputs();
  if (productoId === "" || productoNombre === "" || productoPrecio === "") {
    alert("Falta capturar información");
  } else {
    update(refS(db, 'productos/' + productoId), {
      nombre: productoNombre,
      precio: productoPrecio,
      url: productoURL
    }).then(() => {
      alert("Se actualizó con éxito");
      limpiarInputs();
      mostrarProductos();
    }).catch((error) => {
      alert(error);
    });
  }
}

function eliminarProducto() {
    const id = document.getElementById('id').value;
    remove(refS(db, 'productos/' + id)).then(() => {
      alert("Producto eliminado con éxito");
      limpiarInputs();
      mostrarProductos();
    }).catch((error) => {
      alert("Ocurrió un error al eliminar el producto: " + error);
    });
}

function limpiarInputs() {
    document.getElementById('id').value = '';
  document.getElementById('nombre').value = '';
  document.getElementById('precio').value = '';
  document.getElementById('stock').value = '';
  document.getElementById('url').value = '';
}

function escribirInputs() {
    document.getElementById('id').value = productoId;
    document.getElementById('nombre').value = productoNombre;
    document.getElementById('precio').value = productoPrecio;
    document.getElementById('stock').value = productoStock;
    document.getElementById('url').value = productoURL;
  }

btnBorrar.addEventListener('click', eliminarProducto);
btnAgregar.addEventListener('click', insertarProducto);
btnActualizar.addEventListener('click', actualizarProducto);
btnBuscar.addEventListener('click', buscarProducto);

