import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-auth.js';

const firebaseConfig = {
  apiKey: "AIzaSyBnkQAoCsmxJDoSITf9Bb833hOU9y17XCs",
  authDomain: "aplicacionweb-5d84b.firebaseapp.com",
  databaseURL: "https://aplicacionweb-5d84b-default-rtdb.firebaseio.com",
  projectId: "aplicacionweb-5d84b",
  storageBucket: "aplicacionweb-5d84b.appspot.com",
  messagingSenderId: "930174899745",
  appId: "1:930174899745:web:3ce0cc0d3775d86508b560"
};

const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);

const loginForm = document.getElementById("loginForm");
const emailInput = document.getElementById("username");
const passwordInput = document.getElementById("password");
const signInButton = document.getElementById("signInButton");

signInButton.addEventListener("click", () => {
  const email = emailInput.value;
  const password = passwordInput.value;

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      window.location.href = "/html/agregarproductos.html";
    })
    .catch((error) => {
      console.error(error);
    });
});
