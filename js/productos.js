import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
  apiKey: "AIzaSyBnkQAoCsmxJDoSITf9Bb833hOU9y17XCs",
  authDomain: "aplicacionweb-5d84b.firebaseapp.com",
  databaseURL: "https://aplicacionweb-5d84b-default-rtdb.firebaseio.com",
  projectId: "aplicacionweb-5d84b",
  storageBucket: "aplicacionweb-5d84b.appspot.com",
  messagingSenderId: "930174899745",
  appId: "1:930174899745:web:3ce0cc0d3775d86508b560"
};
 
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
 
window.addEventListener('DOMContentLoaded', (event) => {
    productos();
  });

    function productos() {
    const dbRef = refS(db, 'productos');
    const contenedorProductos = document.getElementById('servicios');
  
    contenedorProductos.innerHTML = '';
  
    onValue(dbRef, (snapshot) => {
      contenedorProductos.innerHTML = '';
  
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
  
        const divServicio = document.createElement('div');
        divServicio.classList.add('servicio');

        const h3Nombre = document.createElement('h3');
        h3Nombre.id = 'nombre';
        h3Nombre.textContent = data.nombre;
        divServicio.appendChild(h3Nombre);
  
 
        const divIconos = document.createElement('div');
        divIconos.classList.add('iconos');
  

        const imgProducto = document.createElement('img');
        imgProducto.src = data.url;
        imgProducto.alt = data.nombre;
        divIconos.appendChild(imgProducto);
        divServicio.appendChild(divIconos);
  

        const pPrecio = document.createElement('p');
        pPrecio.id = 'precio';
        pPrecio.textContent = `Precio: $${data.precio.toString()}`;
        divServicio.appendChild(pPrecio);
  
        const pStock = document.createElement('p');
        pStock.id = 'stock';
        pStock.textContent = `Stock: ${data.stock} unidades`;
        divServicio.appendChild(pStock);

        const divAlinearDerecha = document.createElement('div');
        divAlinearDerecha.classList.add('alinear-derecha');
        divAlinearDerecha.setAttribute('flex', '');
  
        const inputComprar = document.createElement('input');
        inputComprar.classList.add('boton2');
        inputComprar.type = 'submit';
        inputComprar.value = 'Comprar';
        divAlinearDerecha.appendChild(inputComprar);
        divServicio.appendChild(divAlinearDerecha);
  

        contenedorProductos.appendChild(divServicio);
      });
    });
  }